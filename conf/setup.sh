#!/bin/bash
set -a

PATH=$PATH:/home/isucon/local/php/bin/

ln -sf `pwd`/nginx/sites-enabled/nginx.conf /etc/nginx/sites-enabled/nginx.conf
ln -sf `pwd`/nginx/nginx.conf /etc/nginx/nginx.conf

ln -sf `pwd`/php/isubata.php-fpm.conf /home/isucon/local/php/etc/isubata.php-fpm.conf
ln -sf `pwd`/php/php-fpm.conf.default /home/isucon/local/php/etc/php-fpm.conf.default
ln -sf `pwd`/php/php.ini /home/isucon/local/php/etc/php.ini

if [ ! -d "phpredis" ]; then
  git clone https://github.com/phpredis/phpredis.git
  pushd phpredis
  phpize
  ./configure --prefix=/home/isucon/local/php
  make && make install
  popd

  apt-get install redis-server
fi

pushd ../webapp/php
php img_dump.php
popd

mkdir /var/cache/nginx/
