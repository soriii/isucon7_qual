<?php
/* 画像をDBから抽出するやつ */
function getPDO()
{
    static $pdo = null;
    if (!is_null($pdo)) {
        return $pdo;
    }

    $host = 'db';
    $port = getenv('ISUBATA_DB_PORT') ?: '3306';
    $user = 'isucon';
    $password = 'isucon';
    $dsn = "mysql:host={$host};port={$port};dbname=isubata;charset=utf8mb4";

    $pdo = new PDO(
        $dsn,
        $user,
        $password,
        [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    );
    $pdo->query("SET SESSION sql_mode='TRADITIONAL,NO_AUTO_VALUE_ON_ZERO,ONLY_FULL_GROUP_BY'");
    return $pdo;
}

$o = 0;
while (1) {
$stmt = getPDO()->prepare("SELECT id,name,data FROM image LIMIT 10 OFFSET " . $o);
$stmt->execute([]);
$rows = $stmt->fetchall();
if (!$rows) { break; }

foreach($rows as $row) {
    $filename = $row['name'];
    file_put_contents(getcwd().'/imgs/'.$filename, $row['data']);
}
$o += 10;
}
